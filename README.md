# Praktikum Sistem Operasi Modul 1
## _Kelompok A01_
---
#### Anggota Kelompok

| Nama | NRP |
| ------ | ------ |
| Gabrielle Immanuel Osvaldo Kurniawan | 5025211135 |
| Rr. Diajeng Alfisyahrinnisa Anandha | 5025211147 |
| Rayssa Ravelia | 5025211219 |


## ✨ Nomor 1 

### Soal
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 
- Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
- Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas dari hasil filter poin a.
- Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
- Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

### Jawab
Langkah langkah pengerjaan :
1. Download file csv yang ada pada drive modul 1 praktikum sisop 2023
2. Masuk ke folder download dari file csv tersebut menggunakan command sebagai berikut
```c
cd Downloads/
```
Kemudian untuk melakukan check apakah file csv sudah ada dapat menggunakan command :
```c
ls
```
3. Kemudian buat file script yang akan digunakan untuk menyimpan command linux yang sudah dibuat dengan command sebagai berikut :
```c
code university_survey.sh
```
4. Pengerjaan command dilakukan pada script yang sudah dibuat. Dalam pengerjaan digunakan beberapa command yaitu grep, sort, head, echo dan awk. Berikut adalah hasil script command yang dibuat :
```c
#!/bin/bash

echo "Jawaban nomer 1.1 (Top 5 ranking university in japan)"
grep 'Japan' 2023\ QS\ World\ University\ Rankings.csv | sort -n -t, -k1 | head -n 5 | awk -F, '{printf "%-5s %-50s %-10s\n", $1,$2,$4}' 
echo ""

echo "Jawaban nomer 1.2 (Lowest fsr of top 5 ranking university in japan)"
grep 'Japan' 2023\ QS\ World\ University\ Rankings.csv | sort -n -t, -k 9g | head -n 1 | awk -F, '{printf "%-5s %-50s %-10s %-5s\n" ,$1,$2,$4,$9}'
echo ""

echo "Jawaban nomer 1.3 (Top 10 GER of Japan University)"
grep 'Japan' 2023\ QS\ World\ University\ Rankings.csv  | sort -nr -t, -k19 | head -n 10 | awk -F, '{printf "%-50s %-10s %-5s\n" ,$2,$4,$19}'
echo ""

echo "Jawaban nomer 1.4 (Find university name which have substring 'keren')"
awk -F',' '$2 ~ /Keren/ {print $2}' 2023\ QS\ World\ University\ Rankings.csv
```
5. Penjelasan Command
- grep dugunakan untuk mengambil line yang mengandung kata kunci tertentu
- sort -n digunakan untuk mensorting secara numerik. Terdapat 2 cara sorting yaitu :
    1. Ascending (dari kecil ke besar) tanpa menambahkan atribut
    2. Descending (dari besar ke kecil) dengan menambahkan atribut -r pada sort
- head untuk membatasi banyak data yang diambil
- awk untuk memindahkan teks (Dalam soal ini digunakan untuk print sesuai yang diinginkan)
- echo untuk menampilkan teks ke konsol terminal

### Logic penyelesaian:
- [ ] Ambil semua baris yang memiliki kata kunci 'Japan' pada file CSV
- [ ] Sorting sesuai dengan permintaan soal
- [ ] Batasi jumlah data yang diambil
- [ ] Tentukan kolom yang perlu untuk ditampilkan
- [ ] Print menggunakan awk

### Kendala
1. Saat pertama soal 1.2 sedikit ambigu sehingga sebelum H-1 solusi masih salah
    
    Solusi : 
    Revisi jawaban 1.2() Dari fsr terendah top 5 universitas Jepang menjadi fsr terendah semua universitas. perubahan yang dilakukan adalah menghilangkan sorting semula yang dilakukan untuk mengurutkan ranking universitas jepang.

2. Saat revisi 1.2 ditemukan masalah dimana OS tidak dapat membaca kolom 9 sebagai keseluruhan angka karena format tanda '.' dalam file CSV. Kode berikut menghasilkan output yang salah.
```c
grep 'Japan' 2023\ QS\ World\ University\ Rankings.csv | sort -n -t, -k9 | head -n 1 | awk -F, '{printf "%-5s %-50s %-10s %-5s\n" ,$1,$2,$4,$9}'
```
Hasil yang salah : 
![gambar kendalasoal1](./img/K1.png)

Solusi : 
menambahkan flag g pada kolom yang diselect ketika melakukan sort. Sehingga code menjadi seperti berikut :

```
grep 'Japan' 2023\ QS\ World\ University\ Rankings.csv | sort -n -t, -k 9g | head -n 1 | awk -F, '{printf "%-5s %-50s %-10s %-5s\n" ,$1,$2,$4,$9}'
```

### Output :
![gambar soal1](./img/Soal1.png)

## 🌱 Nomor 2 

- Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownoad setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:

    - File yang didownload memiliki format nama "perjalanan_NOMOR.FILE" Untuk NOMOR.FILE adalah urutan file yang didownload (perjalanan_1, perjalanan_2, dst)
    - File batch yang didownload akan dimasukkan ke dalam folder dengan format nama "kumpulan_NOMOR.FOLDER" dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst)

- Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip "devil_NOMOR ZIP" dengan NOMOR.ZIP adaah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP adaah folder kumpulan dari soal diatas 

### List penyelesaian:
1. Code untuk download gambar dari google tentang indonesia
2. Gambar di download sebanyak x (sesuai jam berapa) misal sekarang jam 17 maka download 17 gambar
3. Kalau 00.00 cukup download 1 gambar saja
4. Gambar di download setiap 10 jam sekali mulai dari saat script dijalankan 
5. File yang di download memilki format nama "perjalanan_nomorfile"
6. Semua file dimasukkan ke folder dengan format nama "kumpulan_nomorfolder"
7. Melakukan zip setiap 1 hari dengan nama "devil_nomorzip" 

### Jawaban
Dari logic tersebut, codingan penyelesaian yang terbentuk adalah:

``` c 
# !/bin/bash 

#get tanggal
tanggal=$(date +%d)

#get current directory
dir_awal=$(pwd)

#cek eksistensi directory devil_1
if [ ! -d devil_1 ] && [ ! -e cek.txt ]
then 
    #menjadi boolean untuk eksistensi directory devil_1
    echo "1" > cek.txt

    nomor_zip=1
    mkdir -p devil_$nomor_zip
    path_devil="devil_$nomor_zip"

    nomor_zip=$((nomor_zip + 1))

    echo $tanggal > tanggal.txt
    echo $nomor_zip > zipp.txt

    cd $path_devil

    # Inisialisasi counter setiap ganti zip
    echo "0" > counter.txt
    
else 
    #cek pergantian hari
    if [ $tanggal -ne $(cat tanggal.txt) ]
    then 
        cd $dir_awal
        
        #make zip folder dari devil yang telah ada
        zzipp=$(cat zipp.txt)
        zzipp=$((zzipp - 1))
        path_zip="devil_$zzipp"
        zip -r $path_zip $path_zip

        #menghapus directory tersebut
        rm -r $path_zip

        #memasukkan tanggal ke tanggal.txt
        echo $tanggal > tanggal.txt
        nomor_zip=$(cat zipp.txt)

        #buat directory devil_nomorzip
        mkdir -p devil_$nomor_zip
        path_devil="devil_$nomor_zip"

        nomor_zip=$((nomor_zip + 1))
        echo $nomor_zip > zipp.txt
        
        cd $path_devil

        # Inisialisasi counter setiap ganti zip
        echo "0" > counter.txt

    else 
        nomor_zipp=$(cat zipp.txt)
        nomor_zipp=$((nomor_zipp - 1))
        path_devil="devil_$nomor_zipp"
        cd $path_devil
    fi
fi

#get hour and minute
hour=$(date +%H)

# Cek apakah file telah diakses sebelumnya
if [[ -f "counter.txt" ]]
then
    counter=$(cat counter.txt)
fi

# Tambahkan counter
counter=$((counter + 1))

# Simpan counter kembali ke file
echo $counter > counter.txt

nomor_folder=$counter
mkdir -p kumpulan_$nomor_folder
new_path="kumpulan_$nomor_folder"

#go to new path location
cd $new_path

if [ $hour -eq 00 ]
then 
    #random number
    rand=$(shuf -i 0-100 -n1)

    count=$rand
    query="indonesia"

    #mengekstrak URL gambar dari google images untuk kata kunci di argumen $query
    imagelink=$(wget --user-agent 'Mozilla/5.0' -qO - "https://www.google.com/search?q=${query}\&tbm=isch" | sed 's/</\n</g' | grep '<img' | head -n"$count" | tail -n"$counter" | sed 's/.*src="\([^"]*\)".*/\1/')

    #mengunduh file gambar dari URL pada variable imagelink
    $(wget -qO perjalanan_1 $imagelink)

else 
    nomor_file=1

    until [ ! $nomor_file -le $hour ]
    do 
        #random number
        rand=$(shuf -i 0-200 -n1)

        count=$rand
        query="indonesia"

        #mengekstrak URL gambar dari google images untuk kata kunci di argumen $query
        imagelink=$(wget --user-agent 'Mozilla/5.0' -qO - "https://www.google.com/search?q=${query}\&tbm=isch" | sed 's/</\n</g' | grep '<img' | head -n"$count" | tail -n"$counter" | sed 's/.*src="\([^"]*\)".*/\1/')

        #mengunduh file gambar dari URL pada variable imagelink
        $(wget -qO perjalanan_$nomor_file $imagelink) 

        nomor_file=$((nomor_file + 1))
    done
fi

#kembali ke path awal
cd $dir_awal


#cronjob 
# 0 */10 * * * cd /home/dhe/Documents/Diajeng/Programming/sisop/Praktikum1/soal2 && /bin/bash /home/dhe/Documents/Diajeng/Programming/sisop/Praktikum1/soal2/kobeni_liburan.sh

```

### Penjelasan lebih detail mengenai code

1. Karena kita ingin download gambar tentang Indonesia, maka syntaxnya adalah

``` c
#random number
    rand=$(shuf -i 0-200 -n1)

    count=$rand
    query="indonesia"

#mengekstrak URL gambar dari google images untuk kata kunci di argumen $query
    
    imagelink=$(wget --user-agent 'Mozilla/5.0' -qO - "https://www.google.com/search?q=${query}\&tbm=isch" | sed 's/</\n</g' | grep '<img' | head -n"$count" | tail -n"$counter" | sed 's/.*src="\([^"]*\)".*/\1/')

#mengunduh file gambar dari URL pada variable imagelink
    $(wget -qO perjalanan_$nomor_file $imagelink)

```
> Di dalam `imagelink` kita ekstrak url gambar

> `user-agent` berfungsi untuk mengambil gambar dari browser mozilla

> `-q0` berfungsi untuk mengambil output dari permintaan dan menampilkan ke layar

> Di dalam url kita tetapkan `query` yang kita tetapkan dengan `query = indonesia`

> `sed 's/</\n</g'` berfungsi untuk memecah output menjadi baris-baris yang terpisah

> `grep '<img' ` berfungsi untuk mencari setiap baris yang diawali dengan karakter "<img"

> `head -n"$count"` berfungsi untuk membatasi jumlah baris yang ditampilkan yang ditentukan oleh variabel $count

> `tail -n"$counter"` berfungsi untuk memilih baris yang akan digunakan sebagai URL gambar dan ditentukan oleh variabel $counter

> `sed 's/.*src="\([^"]*\)".*/\1/'` berfungsi untuk mencari URL gambar yang ada pada tag img. 

> Lalu kita masukkan syntax untuk mendownload url yang telah dibuat dalam  `imagelink` sehingga `$(wget -qO perjalanan_$nomor_file $imagelink)`


2. Gambar di download sebanyak x (sesuai jam berapa) misal sekarang jam 17 maka download 17 gambar. Lalu, untuk jam 00 maka hanya mendownload 1 gambar. Sehingga:

``` c
if [ $hour -eq 00 ]
then 
    #random number
    rand=$(shuf -i 0-100 -n1)

    count=$rand
    query="indonesia"

    #mengekstrak URL gambar dari google images untuk kata kunci di argumen $query
    imagelink=$(wget --user-agent 'Mozilla/5.0' -qO - "https://www.google.com/search?q=${query}\&tbm=isch" | sed 's/</\n</g' | grep '<img' | head -n"$count" | tail -n"$counter" | sed 's/.*src="\([^"]*\)".*/\1/')

    #mengunduh file gambar dari URL pada variable imagelink
    $(wget -qO perjalanan_1 $imagelink)

else 
    nomor_file=1

    until [ ! $nomor_file -le $hour ]
    do 
        #random number
        rand=$(shuf -i 0-200 -n1)

        count=$rand
        query="indonesia"

        #mengekstrak URL gambar dari google images untuk kata kunci di argumen $query
        imagelink=$(wget --user-agent 'Mozilla/5.0' -qO - "https://www.google.com/search?q=${query}\&tbm=isch" | sed 's/</\n</g' | grep '<img' | head -n"$count" | tail -n"$counter" | sed 's/.*src="\([^"]*\)".*/\1/')

        #mengunduh file gambar dari URL pada variable imagelink
        $(wget -qO perjalanan_$nomor_file $imagelink) 

        nomor_file=$((nomor_file + 1))
    done
fi
```
> Terdapat if condition `$hour -eq 00` yang berarti apabila jam = 00 maka hanya print 1 gambar sehingga hanya ada `perjalanan_1` pada

``` c
$(wget -qO perjalanan_1 $imagelink)
```

> Terdapat else condition, yang berarti selain jam 00, gambar akan didownload sebanyak jam tersebut. Sehingga, kita gunakan until loop. 
``` c
until [ ! $nomor_file -le $hour ]
```

> Dalam else condition juga terdapat keterangan bahwa nomor perjalanan juga bertambah yang bergantung pada variabel `nomor_file`

3. Semua file dimasukkan ke folder dengan format nama "kumpulan_nomorfolder". Sehingga:

``` c
# Cek apakah file telah diakses sebelumnya
if [[ -f "counter.txt" ]]
then
    counter=$(cat counter.txt)
fi

# Tambahkan counter
counter=$((counter + 1))

# Simpan counter kembali ke file
echo $counter > counter.txt

nomor_folder=$counter
mkdir -p kumpulan_$nomor_folder
new_path="kumpulan_$nomor_folder"

```

> kita disini membutuhkan file `counter.txt` sebagai file penyimpanan variabel dari berapa kali file di akses. Karena, saat kita `bash`, baik dalam `cronjob` atau bash manual, maka kita harus tahu berapa kali file tersebut di akses. Agar nama folder `kumpulan_nomorfolder` selalu bertambah setiap kali diakses. 

4. Karena kita harus melakukan zip setiap 1 hari dengan nama folder `devil_nomorzip` maka:

``` c
#cek eksistensi directory devil_1
if [ ! -d devil_1 ] && [ ! -e cek.txt ]
then 
    #menjadi boolean untuk eksistensi directory devil_1
    echo "1" > cek.txt

    nomor_zip=1
    mkdir -p devil_$nomor_zip
    path_devil="devil_$nomor_zip"

    nomor_zip=$((nomor_zip + 1))

    echo $tanggal > tanggal.txt
    echo $nomor_zip > zipp.txt

    cd $path_devil

    # Inisialisasi counter setiap ganti zip
    echo "0" > counter.txt
    
else 
    #cek pergantian hari
    if [ $tanggal -ne $(cat tanggal.txt) ]
    then 
        cd $dir_awal
        
        #make zip folder dari devil yang telah ada
        zzipp=$(cat zipp.txt)
        zzipp=$((zzipp - 1))
        path_zip="devil_$zzipp"
        zip -r $path_zip $path_zip

        #menghapus directory tersebut
        rm -r $path_zip

        #memasukkan tanggal ke tanggal.txt
        echo $tanggal > tanggal.txt
        nomor_zip=$(cat zipp.txt)

        #buat directory devil_nomorzip
        mkdir -p devil_$nomor_zip
        path_devil="devil_$nomor_zip"

        nomor_zip=$((nomor_zip + 1))
        echo $nomor_zip > zipp.txt
        
        cd $path_devil

        # Inisialisasi counter setiap ganti zip
        echo "0" > counter.txt

    else 
        nomor_zipp=$(cat zipp.txt)
        nomor_zipp=$((nomor_zipp - 1))
        path_devil="devil_$nomor_zipp"
        cd $path_devil
    fi
fi
```
> Terdapat if condition `if [ ! -d devil_1 ] && [ ! -e cek.txt ]`. Awalnya, kita cek dulu eksistensi `devil_1` dan juga memasukkan penanda ke file `cek.txt` apabila file `devil_1` telah terbentuk. Hal itu disebabkan oleh apabila kita telah zip `devil_1`, maka otomatis folder `devil_1` harus kita hapus. Lalu, apabila kita ingin zip `devil_2` maka nomor folder yang terbentuk adalah `devil_1.zip`. Untuk menghindari itu, kita membutuhkan file `cek.txt` sebagai penanda. Sehingga, 

``` c
#menjadi boolean untuk eksistensi directory devil_1
echo "1" > cek.txt
```

> Dalam if condition, kita buat penomoran menjadi `devil_1`

``` c
nomor_zip=1
mkdir -p devil_$nomor_zip
path_devil="devil_$nomor_zip"

nomor_zip=$((nomor_zip + 1))
```

> Disini juga kita masukkan tanggal hari ini ke `tanggal.txt`

``` c
echo $tanggal > tanggal.txt
```
> Kita masukkan penambahan  `nomor_zip` ke file `zipp.txt`

``` c
echo $nomor_zip > zipp.txt
```

> Lalu kita masuk ke path `path_devil`, karena `kumpulan_nomorfolder` akan terbentuk di dalam folder `devil`

> Dalam else condition, kita juga lakukan pengecekan pergantian hari dengan 

``` c
[ $tanggal -ne $(cat tanggal.txt) ]
```
> Apabila terjadi pergantian tanggal, maka kita lakukan zip

``` c
#make zip folder dari devil yang telah ada
zzipp=$(cat zipp.txt)
zzipp=$((zzipp - 1))
path_zip="devil_$zzipp"
zip -r $path_zip $path_zip

#menghapus directory tersebut
rm -r $path_zip
```

> Kita masukkan tanggal terbaru tersebut ke file `tanggal.txt` lagi

``` c
#memasukkan tanggal ke tanggal.txt
echo $tanggal > tanggal.txt
```

> Lalu, kita buat folder devil baru

``` c
nomor_zip=$(cat zipp.txt)

#buat directory devil_nomorzip
mkdir -p devil_$nomor_zip
path_devil="devil_$nomor_zip"

nomor_zip=$((nomor_zip + 1))
echo $nomor_zip > zipp.txt
```

> Lalu, program akan dijalankan di dalam path `path_devil`

``` c
cd $path_devil
```

> Lalu, kita setiap masuk ke path devil, tentunya nomor pada `kumpulan_nomorfolder` harus di set 0 lagi, sehingga

``` c
# Inisialisasi counter setiap ganti zip
echo "0" > counter.txt
```

> Apabila tidak terjadi pergantian jam, maka kita tinggal masuk ke `path_devil` yang telah terbentuk

``` c 
nomor_zipp=$(cat zipp.txt)
nomor_zipp=$((nomor_zipp - 1))
path_devil="devil_$nomor_zipp"
cd $path_devil
```
5. Karena program akan dijalankan setiap 10 jam sekali, maka kita butuhkan `cronjob` sehingga

``` c
0 */10 * * * cd /home/dhe/Documents/Diajeng/Programming/sisop/Praktikum1/soal2 && /bin/bash /home/dhe/Documents/Diajeng/Programming/sisop/Praktikum1/soal2/kobeni_liburan.sh
```

### Logic penyelesaian

Saat melakukan `bash`, kita berada di directory awal. Lalu, kita buat folder `devil_1` karena kita masih melakukan bash pertama kali. Kita ganti current path dengan `cd $path_devil` yang berarti kita sekarang berada di dalam directory `devil_1`

Lalu, karena kita masih melakukan bash 1 kali maka akan terbentuk `kumpulan_1` karena `kumpulan_nomorfolder` bergantung pada berapa kali file diakses. Setelah itu, akan di download gambar sebanyak x jam yang akan tersimpan dengan nama `perjalanan_nomorfile`. Setelah selesai, akan kembali ke directory awal tempat folder devil -> folder kumpulan -> file perjalanan tersimpan. 

Lalu, apabila dilakukan bash 2 kali, maka otomatis `kumpulan_2` akan terbentuk dengan isi x kali `perjalanan_nomorfile`. Hal tersebut akan terus dilakukan hingga terjadi pergantian hari. Apabila terdeteksi telah terjadi pergantian hari (melalui perbandingan tanggal yang berbeda), maka otomatis folder devil sebelumnya akan terhapus dan diganti dengan zip lalu terbentuk folder devil baru. 

### Hasil screenshot 

saat ini jam 7

1. Saat bash pertama kali 

![Bash pertama kali](./img/bashpertama.png)

2. Saat bash kedua kali dan seterusnya akan berlaku sama asalkan masih di tanggal yang sama.

![Bash kedua kali](./img/bashkedua.png)

3. Saat terjadi pergantian hari maka akan terbentuk file zip. 

![Pergantian hari](./img/pergantianhari.png)

#### Kendala dalam Pengerjaan
Selama mengerjakan soal ini kami belum menemukan kendala apapun.

## 🦄 Nomor 3 

### Soal
Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh
- Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut:
    1. Minimal 8 karakter
    2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
    3. Alphanumeric
    4. Tidak boleh sama dengan username 
    5. Tidak boleh menggunakan kata chicken atau ernie
- Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
    1. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
    2. Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
    3. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
    4. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

### Jawab
Pada soal tersebut kita diminta untuk membuat **sistem register** yang akan dibuat dalam script **louis.sh** dan **sistem login** di dalam script **retep.sh**. Semua data user yang berisi username dan password disimpan dalam file /users/users.txt. Adapun kriteria password yang harus dipenuhi seperti yang telah tertera pada soal, serta kita juga harus mencatat percobaan login dan register di dalam file **log.txt** sesuai dengan format file dan message pada log tersebut.

Untuk memenuhi permintaan soal tersebut, berikut ini adalah tahapan yang kami lakukan untuk mengerjakan soal ini.
#### Membuat sistem register pada **louis.sh**

Untuk membuat sistem register sesuai ketentuan maka kita perlu untuk membuat file **users.txt** dan **log.txt** terlebih dahulu yang disimpan dalam **/users**. Untuk mengakses root maka kita perlu menggunakan command **sudo** di dalam perintah. Apabila file **/users** tidak ditemukan maka kita buat perintah untuk membuat direktori tersebut, dan dari direktori tersebut ditambahkan file **users.txt** dan **log.txt**. Tidak lupa untuk mengatur akses permission dengan menggunakan **chmod 777**.

Selanjutnya kita akan membuat script bash untuk melakukan registrasi akun dengan memvalidasi username dan password yang diinputkan oleh user. Berikut ini adalah **tahapannya**:
- Menampilkan pesan sambutan kepada user.
- Membaca input username dari user dan menyimpannya dalam variabel username.
- Melakukan validasi bahwa username yang diinputkan harus unik. Jika username sudah ada di dalam file /users/users.txt, maka akan diminta untuk memasukkan username yang berbeda. Pesan error juga akan dicatat ke dalam file /users/log.txt.
- Melakukan validasi password yang diinputkan oleh user. Loop ini akan berjalan sampai semua kriteria password terpenuhi.
- Membaca input password dari user dan menyimpannya dalam variabel password. Opsi -s digunakan agar input password tidak terlihat di layar dan opsi -p digunakan untuk     menampilkan prompt saat menginput password.
- Melakukan validasi bahwa panjang password harus minimal 8 karakter.
- Melakukan validasi bahwa password harus mengandung minimal satu huruf besar dan satu huruf kecil.
- Melakukan validasi bahwa password hanya boleh terdiri dari karakter alphanumeric (huruf dan angka).
- Melakukan validasi bahwa password tidak boleh sama dengan username.
- Melakukan validasi bahwa password tidak boleh mengandung kata "chicken" atau "ernie".
- Mengubah nilai variabel password_valid menjadi true jika semua kriteria password terpenuhi.
- Menyimpan username dan password ke dalam file /users/users.txt.
- Menampilkan pesan sukses kepada user.
- Mencatat ke dalam file /users/log.txt bahwa user berhasil melakukan registrasi.

Dari tahapan tersebut maka dapat dibuat script bash **louis.sh** sebagai berikut:

```c
#!/bin/bash

#membuat file users.txt dan log.txt dalam /users
if [ ! -d "/users" ]
then 
    sudo mkdir /users
    sudo touch /users/users.txt
    sudo touch /users/log.txt
    sudo chmod 777 /users/users.txt
    sudo chmod 777 /users/log.txt
fi

echo "Welcome to the account registration page"
read -p "Enter your username: " username

# Validate unique username
while [[ $(grep -i "^$username:" /users/users.txt) ]]; do
    read -p "Username is already taken. Please enter a different username: " username
echo "$(date +"%Y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" >> /users/log.txt
done

# Validate password
password_valid=false

while [[ $password_valid == false ]]; do
    read -sp "Enter a new password: " password
    echo ""

    # Check password length
    if [[ ${#password} -lt 8 ]]; then
        echo "Password must be at least 8 characters long"
        continue
    fi

    # Check if password has at least 1 uppercase letter and 1 lowercase letter
    if ! [[ "$password" =~ [[:upper:]] && "$password" =~ [[:lower:]] ]]; then
        echo "Password must have at least 1 uppercase letter and 1 lowercase letter"
        continue
    fi

    # Check if password is alphanumeric
    if ! [[ "$password" =~ ^[[:alnum:]]+$ ]]; then
        echo "Password can only contain alphanumeric characters"
        continue
    fi

    # Check if password is not the same as the username
    if [[ "$password" == "$username" ]]; then
        echo "Password cannot be the same as the username"
        continue
    fi

    # Check if password contains the words "chicken" or "ernie"
    if [[ "$password" =~ (chicken|ernie) ]]; then
        echo "Password cannot contain the words chicken or ernie"
        continue
    fi

    # If all requirements are met, accept the password
    password_valid=true
done

# Save the username and password to a file
echo "$username:$password" >> /users/users.txt
echo "Account registration successful"
echo "$(date +"%Y/%m/%d %H:%M:%S") REGISTER: INFO User $username registered successfully" >> /users/log.txt

```

Berikut ini adalah penjelasan lebih detail mengenai kode di atas:

```c
echo "Welcome to the account registration page"
```
> Pada baris ini, pesan "Welcome to the account registration page" akan dicetak ke layar saat program dimulai. Perintah ```echo``` digunakan untuk menampilkan pesan di layar. Pada kode tersebut, pesan yang ditampilkan adalah ```Welcome to the account registration page```.


```c
echo read -p "Enter your username: " username

```
> Baris ini akan meminta pengguna untuk memasukkan username baru. Perintah ```echo``` digunakan untuk **menampilkan** pesan ```Enter your username:``` di layar. Kemudian, perintah ```read``` digunakan untuk **membaca** input dari pengguna dan menyimpannya ke dalam variabel ```username```.


```c
while [[ $(grep -i "^$username:" /users/users.txt) ]]; do
    read -p "Username is already taken. Please enter a different username: " username
    echo "$(date +"%Y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" >> /users/log.txt
done
```

> Setelah pengguna memasukkan nama pengguna baru, program akan **memeriksa** apakah username tersebut sudah digunakan atau belum di file ```/users/users.txt```. Jika username sudah ada, program akan meminta pengguna untuk memasukkan username yang berbeda dan akan mencatat pesan error ke dalam file ```/users/log.txt```. Perulangan akan terus berlangsung hingga username yang unik ditemukan.

> Dalam detailnya, perintah ```grep``` digunakan untuk **mencari** baris pada file ```users.txt``` yang cocok dengan pola yang diberikan. Pada kasus ini, pola yang digunakan adalah ```^$username:```, yang mencari baris di mana username di **awal** baris sama dengan variabel ```username``` yang diberikan oleh pengguna.

> Perintah ```while``` digunakan untuk membuat **loop** yang akan berjalan selama baris yang cocok ditemukan pada file ```users.txt```. Tanda kurung siku ```( [[ ]] )``` digunakan untuk **mengevaluasi kondisi dalam loop while**, dan tanda titik koma ``( ; ) ``digunakan untuk** memisahkan perintah**.

```c
password_valid=false

while [[ $password_valid == false ]]; do
    read -sp "Enter a new password: " password
    echo ""

    # Check password length
    if [[ ${#password} -lt 8 ]]; then
        echo "Password must be at least 8 characters long"
        continue
    fi

    # Check if password has at least 1 uppercase letter and 1 lowercase letter
    if ! [[ "$password" =~ [[:upper:]] && "$password" =~ [[:lower:]] ]]; then
        echo "Password must have at least 1 uppercase letter and 1 lowercase letter"
        continue
    fi

    # Check if password is alphanumeric
    if ! [[ "$password" =~ ^[[:alnum:]]+$ ]]; then
        echo "Password can only contain alphanumeric characters"
        continue
    fi

    # Check if password is not the same as the username
    if [[ "$password" == "$username" ]]; then
        echo "Password cannot be the same as the username"
        continue
    fi

    # Check if password contains the words "chicken" or "ernie"
    if [[ "$password" =~ (chicken|ernie) ]]; then
        echo "Password cannot contain the words chicken or ernie"
        continue
    fi

    # If all requirements are met, accept the password
    password_valid=true
done

```
> Setelah pengguna memasukkan nama pengguna yang unik, program akan meminta pengguna untuk memasukkan kata sandi baru. Password yang dimasukkan oleh pengguna akan divalidasi oleh serangkaian perintah ```if``` dan perulangan ```while```. Jika password yang dimasukkan tidak memenuhi persyaratan, program akan mencetak pesan kesalahan dan meminta pengguna untuk memasukkan kata sandi yang benar. Jika password yang dimasukkan memenuhi semua persyaratan, program akan menerima password dan lanjut ke bagian selanjutnya.

> Pertama, variabel ```password_valid``` diatur menjadi ```false```, yang menunjukkan bahwa password yang valid** belum dimasukkan**. Loop while kemudian dimulai, dan pengguna diminta untuk memasukkan password baru dengan perintah ```read```.

> Untuk syarat pertama, yaitu panjang password minimal 8 karakter. Operator ```${#password} ```digunakan untuk **mengambil panjang karakter** dari variabel ```password```. Kemudian, operator ```-lt``` digunakan untuk **membandingkan panjang karakter** password dengan angka **8**. Jika panjang karakter password kurang dari 8, maka kondisi if akan terpenuhi, dan pesan kesalahan akan ditampilkan.

> Pada syarat kedua, password minimal memiliki 1 huruf kapital dan 1 huruf kecil. Operator ```&&``` digunakan untuk **mengevaluasi dua kondisi** secara bersamaan. Pada bagian pertama, ```[[ "$password" =~ [[:upper:]] ]]``` akan mengevaluasi apakah password mengandung setidaknya **satu huruf besar**. Pada bagian kedua, ```[[ "$password" =~ [[:lower:]] ]]``` akan mengevaluasi apakah password mengandung setidaknya **satu huruf kecil**. Jika salah satu atau kedua kondisi tidak terpenuhi, maka kondisi if akan terpenuhi, dan pesan kesalahan akan ditampilkan.

> Di dalam syarat ketiga, password harus bersifat alfanumerik. Operator ```!``` digunakan untuk ```membalikkan nilai boolean``` dari ekspresi di sebelah kanannya, sehingga if statement akan dieksekusi jika ekspresi tersebut bernilai ```false```. Terdapat juga ```"${password}" =~ ^[[:alnum:]]+$``` yang merupakan sebuah ekspresi **regular expression **(regex) yang memeriksa apakah nilai variabel password hanya terdiri dari karakter alphanumeric, yaitu kombinasi huruf (a-z, A-Z) dan angka (0-9).

> Pada syarat keempat, password tidak boleh sama dengan username. ```${password}" == "${username}``` **membandingkan nilai** dari variabel ```password``` dengan nilai dari variabel ```username```.

> Syarat kelima, password tidak boleh mengandung kata chicken atau ernie. ```${password}" =~ (chicken|ernie)``` adalah sebuah ekspresi regular expression (regex) yang memeriksa apakah nilai variabel password mengandung kata ```chicken``` atau ```ernie```. ```[[...]] ```adalah konstruksi untuk mengevaluasi ekspresi **Boolean** di dalam bash shell, di mana ```[[``` menandakan awal ekspresi dan ```]]``` menandakan akhir ekspresi.


```c
echo "$username:$password" >> /users/users.txt
echo "Account registration successful"
echo "$(date +"%Y/%m/%d %H:%M:%S") REGISTER: INFO User $username
```
> informasi ```username``` dan ```password``` yang telah diinputkan oleh user disimpan kedalam file ```/users/users.txt```, kemudian menampilkan pesan sukses pendaftaran akun pada layar dan mencatat informasi tersebut pada file log ```/users/log.txt``` beserta **timestamp** menggunakan perintah date.

> ```${username}:${password}``` **menggabungkan** nilai dari variabel ```username``` dan ```password``` dengan menggunakan tanda **titik dua** sebagai pemisah. Hasilnya adalah string yang terdiri dari username dan password yang dipisahkan oleh tanda titik dua.

> ```>>``` adalah operator untuk **menambahkan baris baru pada akhir file** tanpa menghapus konten yang sudah ada di dalamnya. Jadi, setiap kali kode ini dijalankan, string ```${username}:${password}``` akan ditambahkan sebagai baris baru pada akhir file ```/users/users.txt```. Pesan ```Account registration successful``` akan ditampilkan ke layar sebagai tanda bahwa proses pendaftaran akun telah berhasil.

> ```$(date +"%Y/%m/%d %H:%M:%S")``` akan menampilkan **tanggal dan waktu** saat kode tersebut dijalankan dengan format **YYYY/MM/DD HH:MM:SS**. String ini ditambahkan ke akhir file ```/users/log.txt``` dengan menuliskan pesan log yang mencatat bahwa proses pendaftaran akun telah berhasil dilakukan dan mencantumkan ```username``` yang terdaftar.

##### Ouput file louis.sh
Apabila program ini dijalankan maka akan dihasilkan output sebagai berikut:

![register welcome page](./img/no3-louis-welcome-page.png)

Kemudian kita dapat isikan username serta password. Untuk melakukan pengecekan password bekerja sesuai dengan permintaan soal dapat kita lakukan uji coba dengan mengisikan ***password tidak sesuai dengan ketentuan*** sebagai ketentuan:

- Password kurang dari 8 karakter : ```Test```


![pw1](./img/no3-louis-pw1.png)

- Tidak memiliki minimal 1 huruf kapital dan 1 huruf kecil : ```PASSWORD``` dan ```password```

![pw2a](./img/no-3-louis-pw2a.png)

Pada gambar diatas diperoleh output saat password yang diisikan adalah ```PASSSWORD```

![pw2b](./img/no3-louis-pw2b.png)

Pada gambar diatas diperoleh output saat password yang diisikan adalah ```password```

- Tidak alphanumeric : ```Password```

![pw3](./img/no3-louis-pw3.png)

- Password sama dengan username : ```selena```

![pw4](./img/no3-louis-pw4.png)

Diperoleh output ini karena password ```selena``` tidak memenuhi kriteria syarat password yang pertama yaitu lebih dari 8 karakter

- Password menggunakan kata chicken atau ernie : ```chickenAyam123``` dan ```ernieAyam123```

![pw5](./img/no-3-louis-pw-ayam.png)

Pada gambar diatas diperoleh output saat password yang diisikan adalah ```chickenAyam123```

![pw6](./img/no-3-louis-pw-ernie.png)


Pada gambar diatas diperoleh output saat password yang diisikan adalah ```ernieAyam123```


Sebagai contoh akun yang benar kita buat username ```selena``` dan password yang memenuhi syarat adalah ```Password123```, maka

![pw success](./img/no-3-louis-pw-success.png)


#### Membuat sistem login pada **retep.sh**

Setelah membuat sistem register, maka langkah selanjutnya kita harus membuat sistem login. Untuk membuat sistem login terdapat **langkah-langkah** yang kami lakukan sebagai berikut untuk memenuhi syarat pada soal:
1. Tampilkan pesan "Welcome to login page"
2. Minta user untuk memasukkan username dengan menampilkan prompt "Enter your username: "
3. Minta user untuk memasukkan password secara rahasia (input tidak ditampilkan pada layar) dengan menampilkan prompt "Enter your password: "
4. Periksa apakah username dan password yang dimasukkan oleh user cocok dengan pengguna yang valid dalam file users.txt dengan menggunakan command grep. Jika cocok, tampilkan pesan "Login successful" dan tambahkan pesan log ke file log.txt dengan format waktu dan informasi pengguna yang berhasil login.
5. Jika tidak cocok, tampilkan pesan "Incorrect username or password" dan tambahkan pesan log ke file log.txt dengan format waktu dan informasi bahwa upaya login gagal.

Berdasarkan alur tersebut maka kami dapat membuat program sebagai berikut:

```c
#!/bin/bash

# prompt the user for their username and password
echo "Welcome to login page"
read -p "Enter your username: " username
read -sp "Enter your password: " password
echo ""

# Check if the username and password match a valid user in the users.txt file
if grep -q "^$username:$password$" /users/users.txt; then
    echo "Login successful"
    echo "$(date +"%Y/%m/%d %H:%M:%S") LOGIN: INFO User $username logged in" >> /users/log.txt

else
    echo "Incorrect username or password"
    echo "$(date +"%Y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user USERNAME" >> /users/log.txt
fi
```

Berikut ini adalah penjelasan kode tersebut secara rinci

```c
echo "Welcome to login page"
```
> Menampilkan pesan selamat datang pada layar. Perintah ```echo``` adalah salah satu perintah yang sering digunakan dalam shell untuk menampilkan teks atau nilai pada layar. Kata ```Welcome to login page``` adalah string atau teks yang akan ditampilkan oleh perintah ```echo```.


```c
read -p "Enter your username: " username
```
> Meminta user untuk memasukkan username. Perintah ```read``` digunakan untuk membaca input dari pengguna dan menyimpannya ke dalam sebuah variabel. Argumen ```-p``` digunakan untuk menampilkan pesan prompt sebelum membaca input dari pengguna.

> Dalam kode ini, pesan prompt yang ditampilkan adalah ```Enter your username: ```. Pengguna kemudian diminta untuk memasukkan nama pengguna mereka, yang akan disimpan dalam variabel ```username```. Setelah pengguna memasukkan nama pengguna mereka, mereka dapat menekan tombol **Enter** untuk menyelesaikan input.

```c
read -sp "Enter your password: " password

````
> Meminta user untuk memasukkan password dengan **mode input diam** (password tidak ditampilkan saat diketikkan). 

> Perintah ```read``` digunakan untuk membaca input dari pengguna dan menyimpannya ke dalam sebuah variabel. Argumen ```-s``` digunakan untuk mengatur **mode sensitif** terhadap karakter input yang diketik oleh pengguna sehingga karakter-karakter yang dimasukkan oleh pengguna **tidak akan ditampilkan di layar**.

> Argumen ```-p``` digunakan untuk menampilkan **pesan prompt sebelum membaca input** dari pengguna. Dalam kode ini, pesan prompt yang ditampilkan adalah ```Enter your password:``` .

> Setelah pengguna memasukkan password mereka, mereka dapat menekan tombol **Enter** untuk menyelesaikan input. Password yang dimasukkan kemudian akan disimpan dalam variabel ```password```.

```c
if grep -q "^$username:$password$" /users/users.txt; then
```
> **Memeriksa** apakah informasi username dan password yang diinputkan sudah terdaftar pada file ```/users/users.txt``` dengan menggunakan perintah ```grep```. Jika cocok, maka program akan melanjutkan ke baris selanjutnya, yaitu menampilkan pesan sukses login dan mencatat informasi tersebut pada file log. Jika tidak cocok, maka program akan menampilkan sesuai dengan kode selanjutnya. 

> Perintah ```grep``` digunakan untuk mencari **teks atau pola** dalam file. Argumen ```-q``` digunakan untuk mengatur **mode pencarian tanpa menampilkan output** pencarian di layar.

> Pola pencarian yang digunakan dalam kode ini adalah ```^$username:$password$```, yang menunjukkan bahwa username dan password yang dicari** harus sesuai dengan format** tertentu. Simbol ```^``` digunakan untuk menunjukkan **awal baris**, sedangkan simbol ```$``` digunakan untuk menunjukkan **akhir baris**.

```c
echo "Login successful
```
> **Menampilkan pesan** sukses login pada layar jika informasi yang diinputkan oleh user cocok dengan data yang terdaftar pada file ```/users/users.txt```


```c
echo "$(date +"%Y/%m/%d %H:%M:%S") LOGIN: INFO User $username logged in" >> /users/log.tx
```
> **Mencatat** informasi sukses login pada file log ```/users/log.txt``` beserta timestamp menggunakan perintah date. Pesan tersebut berisi informasi jenis log (LOGIN: INFO), username yang berhasil login, dan pesan informasi sukses.

> Pada pesan tersebut, perintah ```date``` digunakan untuk memformat waktu saat ini menjadi format **YYYY/MM/DD HH:MM:SS**. Argumen ```+%Y/%m/%d %H:%M:%S``` digunakan untuk mengatur format output.

> Kata-kata ```LOGIN: INFO``` menunjukkan jenis pesan log, yaitu informasi tentang masuknya pengguna ke dalam sistem. Variabel ```$username``` digunakan untuk menampilkan **username** pengguna yang berhasil login ke dalam sistem.

> Simbol ```>>``` digunakan untuk **menambahkan pesan** log ke akhir file ```log.txt``` tanpa menghapus pesan log yang sudah ada sebelumnya.

```c
echo "Incorrect username or password
```
> Menampilkan pesan error pada layar jika informasi yang diinputkan oleh user tidak cocok dengan data yang terdaftar pada file ```/users/users.txt```

```c
echo "$(date +"%Y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user USERNAME" >> /users/log.txt

```
> Mencatat informasi error login pada file log ```/users/log.txt``` beserta timestamp menggunakan perintah date. Pesan tersebut berisi informasi jenis log (LOGIN: ERROR), username yang mencoba login, dan pesan informasi error. Pada kode ini sama seperti kode di atasnya hanya saja digunakan jika user gagal untuk melakukan login.

##### Ouput file retep.sh
Apabila kode dijalankan maka akan diperoleh output:

![login page](./img/no3-louis-login-page.png)

Sebagai contoh kita masukkan password yang **salah** untuk username ```selena``` dengan password ```gomez``` maka diperoleh output sebagai berikut:

![login-wrong](./img/no-3-retep-login-wrong.png)

Untuk login username dan password yang **benar** akan diperoleh output

![login-correct](./img/no-3-retep-log-correct.png)

#### Mengecek file **users.txt**

Setelah sistem login dan registrasi sudah dibuat, maka kita harus mengecek data pada ```users.txt``` yang disimpan pada ```/users/users.txt```. Berdasarkan input akun yang telah kami lakukan tadi, maka isi dari file ```users.txt``` adalah sebagai berikut:

![users](./img/no-3-users.png)

Di dalam file ```users.txt``` hanya terdapat data akun untuk username ```selena``` karena untuk sementara hanya baru diinput data akun untuk username dan password tersebut.

#### Mengecek file **log.txt**

Langkah terakhir adalah kita harus mengecek apakah data log yang telah dibuat disimpan dengan benar sesuai dengan permintaan soal pada saat menjalankan ```louis.sh``` maupun ```retep.sh```. Data log disimpan pada ```/users/log.txt```. Berdasarkan input akun yang telah kami lakukan tadi, maka isi dari file ```log.txt``` adalah sebagai berikut:

![log](./img/no-3-log.png)

Di dalam file ```log.txt``` hanya terdapat pesan pada saat ```REGISTER``` dan ```LOGIN```. Percobaan login yang kita tadi lakukan terdapat dua, yaitu saat salah dan benar sehingga untuk sementara data ```log.txt``` hanya menyimpan ```3 pesan```.

### Kendala dalam Pengerjaan
Selama mengerjakan soal ini kami belum menemukan kendala apapun.

## 🔅 Nomor 4 

### Soal
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan:
- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
    1. Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
    2. Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
    3. Setelah huruf z akan kembali ke huruf a
- Buat juga script untuk dekripsinya.
- Backup file syslog setiap 2 jam untuk dikumpulkan 😀.

### Jawab
Berdasarkan soal tersebut kita diminta untuk melakukan encrypt dan decrypt dari log sistem komputer. Untuk melakukan sistem cipher dilakukan dengan mengubah alphabet dengan menambahkan nilai jam yang ditunjuk saat ini. Data encrypt tersebut disimpan dalam format jam sesuai dengan ketentuan dan dalam format txt, serta program encrypt ditulis dalam **log_encrypt.sh**. Kemudian, kita harus melakukan dekripsi file tersebut yang dibuat dalam file **log_decrypt.sh**. Backup file syslog ini harus dikumpulkan setiap 2 jam.

Untuk memenuhi permintaan soal tersebut, berikut ini adalah tahapan yang kami lakukan untuk mengerjakan soal ini.

#### Membuat file encrypt pada log_encrypt.sh
Langkah pertama kita harus membuat file untuk melakukan encrypt system log terlebih dahulu, yang nantinya akan di decrypt pada tahap berikutnya. Untuk membuat encrypt sesuai dengan permintaan soal, berikut ini adalah logika tahapan penyelesaian yang kami buat:
1. Set variabel alphabet sebagai string abjad kecil a sampai z.
Variabel ini digunakan sebagai acuan untuk mengenkripsi isi file.
2. Mendapatkan nilai variabel hour dengan menggunakan perintah date dengan format jam (HH).
Variabel hour akan menyimpan nilai jam pada saat kode dijalankan.
3. Mendapatkan nilai variabel shift dengan menghitung modulus hour dengan 26.
Variabel shift akan menyimpan nilai sisa hasil bagi hour dengan 26. Hal ini dilakukan agar nilai shift tidak lebih besar dari 25, yang merupakan jumlah huruf dalam alfabet.
3. Mendapatkan isi file syslog
Disimpan dalam variabel content.
4. Mengenkripsi isi variabel content
Mengganti setiap karakter dalam variabel alphabet dengan karakter yang berjarak shift karakter di depannya. Jika karakter sudah mencapai akhir alfabet, maka akan dilanjutkan kembali dari awal alfabet. Hasilnya disimpan dalam variabel encrypted_content.
Untuk mengenkripsi isi variabel content. Modulo 26 digunakan pada tahap sebelumnya agar nilai shift tidak lebih besar dari 25. Jika sudah mencapai huruf z, maka karakter akan dilanjutkan kembali dari huruf a. Hasil enkripsi disimpan dalam variabel encrypted_content.
5. Menuliskan isi variabel encrypted_content ke dalam file dengan format nama "HH:MM_dd:mm:yyyy.txt" 

Berdasarkan logika penyelesaian tersebut dapat dibuat script bash **log_encrypt.sh** sebagai berikut

```c
#!/bin/sh
alphabet="abcdefghijklmnopqrstuvwxyz"
hour=$(date +"%H")
shift=$((hour % 26))

#memakai rhel os
content=$(sudo cat /var/log/messages)
encrypted_content=$(echo "$content" | tr "${alphabet}" "${alphabet:${shift}}${alphabet:0:${shift}}")
echo "$encrypted_content" | sudo tee /var/log/backup_log/encrypt/"$(date +%H:%M_%d:%m:%Y)".txt > /dev/null

#cronjob
#0 */2 * * * sh /var/log/backup_log/log_encrypt.sh
```c

Berikut ini adalah penjelasan yang lebih detail mengenai kode di atas:
```c
alphabet="abcdefghijklmnopqrstuvwxyz"
```
> Baris ini mendefinisikan variabel ```alphabet``` sebagai string abjad kecil a sampai z. Variabel ini akan digunakan sebagai **acuan untuk mengenkripsi** isi file.

```c
hour=$(date +"%H")
```
> Baris ini mendefinisikan variabel ```hour``` dengan menggunakan perintah ```date``` dengan format jam (HH). Variabel hour akan **menyimpan nilai jam** pada saat kode dijalankan.

> Perintah ```date``` adalah perintah di Linux yang digunakan untuk **menampilkan atau mengatur** tanggal dan waktu sistem. Ketika perintah date dieksekusi tanpa argumen, ia akan menampilkan **tanggal dan waktu** sistem **saat ini**.

> Opsi format ```%H``` pada perintah date menentukan format output waktu yang diinginkan, yaitu hanya menampilkan **jam dalam format 24 jam (00-23)**.

> Dalam kode tersebut, hasil output dari perintah ```date``` yang berupa **jam** dalam format 24 jam tersebut akan disimpan dalam variabel ```hour```. Variabel hour kemudian akan digunakan untuk menghitung nilai shift dalam ```teknik shift cipher```.


```c
shift=$((hour % 26))
```
> Baris  ini mendefinisikan variabel ```shift``` dengan menghitung **modulus hour dengan 26**. Variabel ```shift``` akan menyimpan **nilai sisa hasil bagi** hour dengan 26. Hal ini dilakukan agar nilai shift tidak lebih besar dari 25, yang merupakan** jumlah huruf dalam alfabet.**

> Operator modulus ```% ```digunakan untuk mendapatkan **sisa hasil bagi** dari suatu bilangan. Dalam hal ini, operator modulus digunakan untuk menghitung sisa hasil bagi antara nilai variabel hour dengan 26.

> Mengapa harus menggunakan modulus 26? Karena dalam teknik shift cipher, kunci atau nilai shift adalah bilangan bulat yang digunakan untuk menggeser huruf dalam alfabet. Jumlah huruf dalam alfabet dalah **26**. Oleh karena itu, untuk mengenkripsi suatu pesan dengan teknik shift cipher, nilai shift yang digunakan harus berada dalam rentang 0 sampai 25 (26 nilai yang mungkin). Jika nilai shift lebih dari 25, maka hasil penggeseran huruf akan melebihi jumlah huruf dalam alfabet dan nilai tersebut harus dikurangi dengan 26 agar **kembali ke dalam rentang** yang valid.

```c
content=$(sudo cat /var/log/messages)
```
> Baris ini mendefinisikan variabel ```content``` dengan mengambil isi dari file ```/var/log/messages``` menggunakan perintah ```sudo cat```. Variabel ```content``` akan menyimpan isi dari file tersebut. Perintah ```sudo``` digunakan agar dapat mengakses file tersebut dengan hak akses **superuser**. ```content``` kemudian akan digunakan sebagai **input** untuk proses **enkripsi** menggunakan teknik shift cipher.

> Pada beberapa distribusi Linux, seperti **Ubuntu**, file ```/var/log/messages``` tidak digunakan untuk menyimpan log sistem operasi. Sebaliknya, log sistem operasi disimpan dalam file ```/var/log/syslog```. Oleh karena itu, pada distribusi Linux seperti Ubuntu, baris kode ```content=$(sudo cat /var/log/messages)``` akan diganti dengan baris kode yang sesuai dengan jenis log yang akan dienkripsi. Misalnya, jika log yang ingin dienkripsi adalah log sistem operasi, maka baris kode tersebut dapat diganti dengan ```content=$(sudo cat /var/log/syslog)```.

```c
encrypted_content=$(echo "$content" | tr "${alphabet}" "${alphabet:${shift}}${alphabet:0:${shift}}")
```
> Baris ini mendefinisikan variabel ```encrypted_content``` dengan **mengenkripsi** isi variabel content menggunakan teknik shift cipher dengan menggunakan shift sebanyak nilai variabel shift yang telah dihitung sebelumnya. 

> Untuk mengenkripsi isi variabel content. Dalam kode ini, proses enkripsi dilakukan menggunakan perintah ```tr``` di Linux, yaitu perintah yang digunakan untuk melakukan **translasi karakter** dalam teks.

> Digunakan perintah ```tr``` yang akan mengubah setiap karakter dalam variabel content sesuai dengan karakter yang berjarak **shift karakter** di depannya dalam variabel alphabet. Modulo 26 digunakan pada tahap sebelumnya agar nilai shift tidak lebih besar dari 25. Jika sudah mencapai huruf z, maka karakter akan dilanjutkan kembali dari huruf a. Hasil enkripsi disimpan dalam variabel ```encrypted_content```.

> ```${alphabet:${shift}}${alphabet:0:${shift}}``` adalah **alfabet yang sudah digeser **sebanyak nilai shift. Bagian ```${alphabet:${shift}}``` berarti mengambil karakter dari **indeks ke-n** dalam alfabet, dimulai dari karakter ke-n hingga akhir alfabet, sedangkan ```${alphabet:0:${shift}}``` berarti mengambil karakter dari **indeks ke-nol hingga indeks ke-(n-1)** dalam alfabet.

> Pada proses translasi, setiap **huruf dalam pesan** akan diganti dengan huruf yang sesuai dengan **posisi huruf** tersebut dalam alfabet yang sudah digeser. Misalnya, jika nilai shift adalah 3, maka huruf a akan diganti dengan huruf d, huruf b dengan huruf e, dan seterusnya.

```c
echo "$encrypted_content" | sudo tee /var/log/backup_log/encrypt/"$(date +%H:%M_%d:%m:%Y)".txt > /dev/null
```
> Baris terakhir ini menuliskan isi variabel ```encrypted_content``` ke dalam file dengan format nama **HH:MM_dd:mm:yyyy.txt** di direktori ```/var/log/backup_log/encrypt ```dengan menggunakan perintah ```sudo tee```. Nama file dibentuk berdasarkan format **HH:MM_dd:mm:yyyy.txt**, yang merupakjalan waktu saat kode diankan. Output dari perintah tee diarahkan ke ```/dev/null``` agar tidak ditampilkan pada terminal.

> Hasil enkripsi yang disimpan dalam variabel ```encrypted_content``` **dicetak** menggunakan perintah ```echo```. Output dari perintah echo kemudian dialirkan menggunakan operator ```|``` (pipe) ke perintah ```sudo tee```. 

> Perintah ```tee``` berguna untuk** menyalin output dari sebuah perintah dan menyimpannya** ke dalam sebuah file, sementara perintah ```sudo``` digunakan untuk memperoleh hak akses superuser yang diperlukan untuk menyimpan file di dalam direktori ```/var/log/backup_log/encrypt```.

> Selanjutnya, file baru akan disimpan di direktori ```/var/log/backup_log/encrypt``` dengan nama file yang sesuai dengan waktu enkripsi. Nama file dibentuk menggunakan perintah ```$(date +%H:%M_%d:%m:%Y)```, yang menghasilkan format waktu **HH:MM_DD:MM:YYYY**.

> Operator ```>``` digunakan untuk mengalihkan output dari perintah ```sudo tee``` ke **file baru**. Namun, karena kita tidak perlu melihat hasil dari output tersebut, maka output tersebut diarahkan ke ```/dev/null```, yaitu sebuah "file" khusus di Linux yang tidak menuliskan apapun yang diberikan padanya. Dengan demikian, output dari perintah ```sudo tee``` **tidak akan ditampilkan** pada layar dan hanya **disimpan** ke dalam file baru.

##### Output file log_encrypt.sh 
Apabila program ini dijalankan maka tidak akan dihasilkan output apa pun sebagai berikut:

![enc-output](./img/no-4-enc-output.png)


#### Membuat file log_decrypt.sh
Setelah membuat file untuk encrypt, maka langkah selanjutnya kita harus membuat file untuk melakukan decrypt. Untuk melakukan decrypt berikut ini adalah **alur logika** penyelesaian yang kami buat:
1. Simpan waktu saat ini dalam variabel "hour" dengan format HH menggunakan perintah date
Tetapkan variabel "alphabet" dengan urutan huruf abjad
2. Hitung nilai "shift" dengan mengambil modulus dari variabel "hour" dibagi 26
3. Jika jumlah argumen yang diberikan tidak sama dengan 1, cetak pesan penggunaan seharusnya dan keluar dari skrip
4. Jika file yang ditentukan tidak ada, cetak pesan untuk kesalahan dan keluar dari skrip
5. Baca isi file yang ditentukan dan simpan ke dalam variabel "encrypted_content"
6. Terapkan pergeseran "shift" ke setiap karakter dalam variabel "encrypted_content" menggunakan untuk mengembalikan alphabet ke semula dan simpan hasil dekripsi ke dalam variabel "decrypted_content"
7. Tulis isi "decrypted_content" ke file yang ditentukan

Berdasarkan alur tersebut maka kami dapat membuat program sebagai berikut:

```c
#!/bin/sh
hour=$(date +"%H")
alphabet="abcdefghijklmnopqrstuvwxyz"
shift=$((hour % 26))

# Check if the script was called with one argument
if [ $# -ne 1 ]
then
  echo "Usage: $0 [filename]"
  exit 1
fi

# Check if the specified file exists
if [ ! -f $1 ]
then
  echo "Error: File not found"
  exit 1
fi

encrypted_content=$(cat $1)
decrypted_content=$(echo "$encrypted_content" | tr "${alphabet:${shift}}${alphabet:0:${shift}}" "$
{alphabet}")
echo "$decrypted_content" | sudo tee /var/log/backup_log/dec_syslog > /dev/null
```
Berikut ini adalah penjelasan kode tersebut secara rinci
```c
hour=$(date +"%H")
```
> Baris ini menggunakan perintah ```date``` untuk mengambil waktu saat ini dalam format **jam** (HH) dan menyimpannya ke dalam variabel ```hour```.


```c
alphabet="abcdefghijklmnopqrstuvwxyz"
```
> Baris ini menetapkan variabel ```alphabet``` dengan **urutan huruf abjad**.


```c
shift=$((hour % 26))
```
> Baris ini menghitung nilai ```shift``` dengan mengambil modulus dari variabel ```hour``` dibagi 26. Variabel "shift" ini akan digunakan untuk menggeser setiap huruf dalam teks asli saat melakukan enkripsi dan dekripsi.


```c
if [ $# -ne 1 ]
then
  echo "Usage: $0 [filename]"
  exit 1
fi
```
> Baris ini **memeriksa** apakah script dipanggil dengan satu argumen. Jika tidak, pesan penggunaan akan dicetak dan script akan keluar dengan exit code 1.

> Dalam skrip bash, ```$# ```digunakan untuk **menghitung jumlah argumen** yang diberikan pada saat menjalankan script.

> Jika jumlah argumen yang diberikan tidak sama dengan 1, maka scipt akan mencetak pesan penggunaan dengan menggunakan variabel ```$0``` yang menyimpan nama skrip itu sendiri, yaitu ```Usage: $0 [filename]```. Pesan ini memberitahu pengguna cara menggunakan scipt, yaitu dengan memberikan satu argumen berupa **nama file yang akan di-dekripsi**.

> Selanjutnya, jika jumlah argumen yang diberikan tidak sama dengan 1, scipt akan keluar dengan **exit code 1** menggunakan perintah ```exit 1```. Exit code 1 menandakan bahwa skrip mengalami** kesalahan dan tidak dapat menyelesaikan tugasnya**.

```c
if [ ! -f $1 ]
then
  echo "Error: File not found"
  exit 1
fi
```

> Baris ini memeriksa apakah file yang ditentukan **ada atau tidak**. Jika file tidak ditemukan, pesan kesalahan akan dicetak dan skrip akan keluar dengan exit code 1.

> Dalam skrip bash, ```$1``` digunakan untuk mengambil **argumen pertama yang diberikan ** pada saat menjalankan script.

> Jika file yang diberikan sebagai argumen tidak ada, script akan mencetak pesan kesalahan ```Error: File not found"``` dengan menggunakan perintah ```echo```. Selanjutnya, jika file tidak ditemukan, scipt akan keluar dengan **exit code 1** menggunakan perintah ```exit 1```.


```c
encrypted_content=$(cat $1)
```
> Baris ini **membaca** isi file yang ditentukan dan **menyimpannya** ke dalam variabel ```encrypted_content```.

> Dalam skrip bash, perintah ``cat`` digunakan untuk **membaca dan menampilkan isi** dari sebuah file. Variabel ```$1``` di sini merupakan argumen pertama yang diberikan pada saat menjalankan scipt, yaitu **nama file** yang akan di-dekripsi.

```c
decrypted_content=$(echo "$encrypted_content" | tr "${alphabet:${shift}}${alphabet:0:${shift}}" "${alphabet}")
```

> Baris ini melakukan **dekripsi** teks yang telah dienkripsi sebelumnya dengan menggeser setiap huruf sesuai dengan nilai ```shift``` menggunakan perintah ```tr``` dengan argumen ```${alphabet:${shift}}${alphabet:0:${shift}}```. Hasil dekripsi kemudian disimpan ke dalam variabel ```decrypted_content```.

> Perintah ```tr``` pada baris kode ini berguna untuk melakukan **translasi karakter**, di mana setiap karakter yang ada pada variabel ```encrypted_content``` yang merupakan **alfabet**, akan diganti dengan karakter alfabet lain yang ditentukan oleh variabel ```${alphabet:${shift}}${alphabet:0:${shift}}```. Variabel ini berfungsi untuk membuat alfabet yang **di-shift** sebanyak nilai ```${shift}```.

> Karakter alfabet di dalam variabel``` ${alphabet:${shift}}${alphabet:0:${shift}}``` terdiri dari dua bagian, yaitu``` ${alphabet:${shift}}``` dan ```${alphabet:0:${shift}}```. Bagian pertama ```${alphabet:${shift}}``` mengambil **karakter alfabet** dari urutan ke-```${shift}``` hingga karakter alfabet terakhir, sedangkan bagian kedua ```${alphabet:0:${shift}}``` mengambil **karakter alfabet dari urutan ke-0** hingga ke```-${shift-1}```. Dengan demikian, variabel ini akan memuat alfabet yang di-shift sebanyak nilai ```${shift}```.

> Setelah itu, ```tr``` akan mengganti setiap karakter alfabet yang ada pada variabel ```encrypted_content``` dengan karakter **alfabet lain** yang sesuai dengan variabel ```${alphabet:${shift}}${alphabet:0:${shift}}```. Proses penggantian ini akan menghasilkan hasil dekripsi yang akan disimpan ke dalam variabel ```decrypted_content```.

```c
echo "$decrypted_content" | sudo tee /var/log/backup_log/dec_syslog > /dev/null
```
> Baris ini menulis isi variabel "decrypted_content" ke dalam file ```/var/log/backup_log/dec_syslog```

> Isi file yang telah didekripsi disimpan dalam variabel ```decrypted_content```. Kemudian, perintah ```echo``` digunakan untuk mencetak isi dari variabel decrypted_content.

> Output dari perintah ```echo``` kemudian akan di-pipe ```(|)``` ke perintah ```sudo tee /var/log/backup_log/dec_syslog```. Perintah ```tee``` digunakan untuk** menuliskan output** yang diberikan oleh perintah sebelumnya ke dalam file yang telah ditentukan.

> Pada baris kode ini, perintah ```sudo``` digunakan untuk menjalankan perintah ```tee``` dengan hak akses **superuser**. Hal ini diperlukan karena direktori ```/var/log/backup_log/``` hanya dapat diakses oleh superuser.

> Output dari perintah ```tee``` kemudian akan diarahkan ke ```/dev/null``` menggunakan operator ```>```. ```/dev/null``` adalah file khusus di Linux yang digunakan untuk **menghapus output** dari perintah yang dijalankan. Hal ini dilakukan agar output tidak ditampilkan di layar dan hanya disimpan ke dalam file yang telah ditentukan. Dengan demikian, baris kode ini akan menuliskan isi file yang telah didekripsi ke dalam file **/var/log/backup_log/dec_syslog** tanpa menampilkan output di layar.

##### Output file log_decrypt.sh 
Apabila program ini dijalankan maka akan dihasilkan output apa pun sebagai berikut:

![dec-output](./img/no-4-dec-output.png)

Diperoleh hasil sebagai berikut karena kita harus menuliskan nama file yang mau dilakukan decrypt terlebih dahulu.

#### Mengatur crontab

Untuk dapat melakukan enkripsi setiap 2 jam kita harus mengatur crontab dengan kode sebagai berikut

```c
0 */2 * * * sh /var/log/backup_log/log_encrypt.sh
```
Berikut ini adalah screenshot crontab yang ada pada local  sesuai dengan kode di atas

![crontab](./img/no-4-crontab.png)

> Pada kode tersebut digunakan untuk menjalankan script ```log_encrypt.sh``` setiap **dua jam sekali pada setiap hari**.

> Penjelasan setiap bagian konfigurasi cronjob sebagai berikut:

> 0: Menit ke-0

> */2: Setiap dua jam sekali

> *: Setiap hari dalam bulan

> *: Setiap bulan

> *: Setiap hari dalam minggu

> Kemudian, ```sh /var/log/backup_log/log_encrypt.sh``` adalah perintah yang akan dijalankan oleh crontab. Dalam hal ini, crontab akan menjalankan script ```log_encrypt.sh``` yang terletak pada direktori ```/var/log/backup_log/```.

#### Mengecek file hasil encrypt
Berdasarkan crontab yang sudah diatur maka akan dilakukan enkripsi **setiap 2 jam** selama komputer menyala. Diperoleh hasil encrypt yang disimpan dalam ```/var/log/backup_log/encrypt/``` sebagai berikut setelah kami tingal lebih dari 2 jam dengan kondisi komputer menyala:

![list encrypt](./img/no-4-enc-list.png)

Sebagai contoh hasil encrypt kami membuka file dengan nama ```04:00_10:03:2023.txt``` maka diperoleh hasil encrypt sebagi berikut:

![enc](./img/no-4-enc.png)


#### Mengecek file hasil decrypt
Untuk membuka file hasil decrypt kita harus menjalankan program ```log_decrypt``` dengan pathnya. Untuk uji coba memastikan kode pengecekan file dan format input sesuai yang diinginkan, kami melakukan testing untuk kedua hal tersebut sebagai berikut:
- Output saat salah memasukkan input

![dec-wrong-input](./img/no-4-dec-input-wrong.png)

Diperoleh pesan untuk menunjukkan penggunaan yang sebenarnya yakni harus mencantumkan filename
- Output saat memasukkan file yang tidak ada dalam encrypt

![dec-filename-wrong](./img/no-4-dec-finename-wrong.png)

Diperoleh pesan yang menunjukkan bahwa file yang mau di-decrypt tidak ada dan program langsung keluar

Sebagai contoh yang benar, kami coba untuk melakukan decrypt pada file ```04:00_10:03:2023.txt```, diperoleh output sebagai berikut:

![dec-correct](./img/no-4-enc-correct.png)

Dapat dilihat bahwa program akan langsung keluar ketika input dan filename sesuai dengan yang diminta. Untuk melihat hasil decrypt kita harus menggunakan perintah ```cat``` pada file decrypt sebagai contoh berikut:

![dec-cat-command](./img/no-4-dec-cmd.png)

Maka diperoleh hasil decrypt sebagai berikut

![dec-result](./img/no-4-dec-result.png)

#### Kendala dalam Pengerjaan
Selama mengerjakan soal ini kami belum menemukan kendala apapun.



