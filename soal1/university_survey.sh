#!/bin/bash

cd ~/Downloads/

echo "Jawaban nomer 1.1 (Top 5 ranking university in japan)"
grep 'Japan' 2023\ QS\ World\ University\ Rankings.csv | sort -n -t, -k1 | head -n 5 | awk -F, '{printf "%-5s %-50s %-10s\n", $1,$2,$4}' 
echo ""

echo "Jawaban nomer 1.2 (5 Lowest fsr of university in japan)"
grep 'Japan' 2023\ QS\ World\ University\ Rankings.csv | sort -t, -k 9g | head -n 5 | awk -F, '{printf "%-50s %-10s %-10s\n",$2,$4,$9}' 
echo ""

echo "Jawaban nomer 1.3 (Top 10 GER of Japan University)"
grep 'Japan' 2023\ QS\ World\ University\ Rankings.csv  | sort -nr -t, -k19 | head -n 10 | awk -F, '{printf "%-50s %-10s %-5s\n" ,$2,$4,$19}'
echo ""

echo "Jawaban nomer 1.4 (Find university name which have substring 'keren')"
awk -F',' '$2 ~ /Keren/ {print $2}' 2023\ QS\ World\ University\ Rankings.csv
