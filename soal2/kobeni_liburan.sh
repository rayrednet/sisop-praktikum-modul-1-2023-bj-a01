# !/bin/bash

#get tanggal
tanggal=$(date +%d)

#get current directory
dir_awal=$(pwd)

#cek eksistensi directory devil_1
if [ ! -d devil_1 ] && [ ! -e cek.txt ]
then 
    #menjadi boolean untuk eksistensi directory devil_1
    echo "1" > cek.txt

    nomor_zip=1
    mkdir -p devil_$nomor_zip
    path_devil="devil_$nomor_zip"

    nomor_zip=$((nomor_zip + 1))

    echo $tanggal > tanggal.txt
    echo $nomor_zip > zipp.txt

    cd $path_devil

    # Inisialisasi counter setiap ganti zip
    echo "0" > counter.txt
    
else 
    #cek pergantian hari
    if [ $tanggal -ne $(cat tanggal.txt) ]
    then 
        cd $dir_awal
        
        #make zip folder dari devil yang telah ada
        zzipp=$(cat zipp.txt)
        zzipp=$((zzipp - 1))
        path_zip="devil_$zzipp"
        zip -r $path_zip $path_zip

        #menghapus directory tersebut
        rm -r $path_zip

        #memasukkan tanggal ke tanggal.txt
        echo $tanggal > tanggal.txt
        nomor_zip=$(cat zipp.txt)

        #buat directory devil_nomorzip
        mkdir -p devil_$nomor_zip
        path_devil="devil_$nomor_zip"

        nomor_zip=$((nomor_zip + 1))
        echo $nomor_zip > zipp.txt
        
        cd $path_devil

        # Inisialisasi counter setiap ganti zip
        echo "0" > counter.txt

    else 
        nomor_zipp=$(cat zipp.txt)
        nomor_zipp=$((nomor_zipp - 1))
        path_devil="devil_$nomor_zipp"
        cd $path_devil
    fi
fi

#get hour and minute
hour=$(date +%H)

# Cek apakah file telah diakses sebelumnya
if [[ -f "counter.txt" ]]
then
    counter=$(cat counter.txt)
fi

# Tambahkan counter
counter=$((counter + 1))

# Simpan counter kembali ke file
echo $counter > counter.txt

nomor_folder=$counter
mkdir -p kumpulan_$nomor_folder
new_path="kumpulan_$nomor_folder"

#go to new path location
cd $new_path

if [ $hour -eq 00 ]
then 
    #random number
    rand=$(shuf -i 0-100 -n1)

    count=$rand
    query="indonesia"

    #mengekstrak URL gambar dari google images untuk kata kunci di argumen $query
    imagelink=$(wget --user-agent 'Mozilla/5.0' -qO - "https://www.google.com/search?q=${query}\&tbm=isch" | sed 's/</\n</g' | grep '<img' | head -n"$count" | tail -n"$counter" | sed 's/.*src="\([^"]*\)".*/\1/')

    #mengunduh file gambar dari URL pada variable imagelink
    $(wget -qO perjalanan_1 $imagelink)

else 
    nomor_file=1

    until [ ! $nomor_file -le $hour ]
    do 
        #random number
        rand=$(shuf -i 0-200 -n1)

        count=$rand
        query="indonesia"

        #mengekstrak URL gambar dari google images untuk kata kunci di argumen $query
        imagelink=$(wget --user-agent 'Mozilla/5.0' -qO - "https://www.google.com/search?q=${query}\&tbm=isch" | sed 's/</\n</g' | grep '<img' | head -n"$count" | tail -n"$counter" | sed 's/.*src="\([^"]*\)".*/\1/')

        #mengunduh file gambar dari URL pada variable imagelink
        $(wget -qO perjalanan_$nomor_file $imagelink) 

        nomor_file=$((nomor_file + 1))
    done
fi

#kembali ke path awal
cd $dir_awal

#cronjob 
# 0 */10 * * * cd /home/dhe/Documents/Diajeng/Programming/sisop/Praktikum1/soal2 && /bin/bash /home/dhe/Documents/Diajeng/Programming/sisop/Praktikum1/soal2/kobeni_liburan.sh
