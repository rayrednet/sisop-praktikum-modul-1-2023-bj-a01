#!/bin/bash

#membuat file users.txt dan log.txt dalam /users
if [ ! -d "/users" ]
then 
    sudo mkdir /users
    sudo touch /users/users.txt
    sudo touch /users/log.txt
    sudo chmod 777 /users/users.txt
    sudo chmod 777 /users/log.txt
fi

echo "Welcome to the account registration page"
read -p "Enter your username: " username

# Validate unique username
while [[ $(grep -i "^$username:" /users/users.txt) ]]; do
    read -p "Username is already taken. Please enter a different username: " username
echo "$(date +"%Y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" >> /users/log.txt
done

# Validate password
password_valid=false

while [[ $password_valid == false ]]; do
    read -sp "Enter a new password: " password
    echo ""

    # Check password length
    if [[ ${#password} -lt 8 ]]; then
        echo "Password must be at least 8 characters long"
        continue
    fi

    # Check if password has at least 1 uppercase letter and 1 lowercase letter
    if ! [[ "$password" =~ [[:upper:]] && "$password" =~ [[:lower:]] ]]; then
        echo "Password must have at least 1 uppercase letter and 1 lowercase letter"
        continue
    fi

    # Check if password is alphanumeric
    if ! [[ "$password" =~ ^[[:alnum:]]+$ ]]; then
        echo "Password can only contain alphanumeric characters"
        continue
    fi

    # Check if password is not the same as the username
    if [[ "$password" == "$username" ]]; then
        echo "Password cannot be the same as the username"
        continue
    fi

    # Check if password contains the words "chicken" or "ernie"
    if [[ "$password" =~ (chicken|ernie) ]]; then
        echo "Password cannot contain the words chicken or ernie"
        continue
    fi

    # If all requirements are met, accept the password
    password_valid=true
done

# Save the username and password to a file
echo "$username:$password" >> /users/users.txt
echo "Account registration successful"
echo "$(date +"%Y/%m/%d %H:%M:%S") REGISTER: INFO User $username registered successfully" >> /users/log.txt

