#!/bin/bash

# prompt the user for their username and password
echo "Welcome to login page"
read -p "Enter your username: " username
read -sp "Enter your password: " password
echo ""

# Check if the username and password match a valid user in the users.txt file
if grep -q "^$username:$password$" /users/users.txt; then
    echo "Login successful"
    echo "$(date +"%Y/%m/%d %H:%M:%S") LOGIN: INFO User $username logged in" >> /users/log.txt

else
    echo "Incorrect username or password"
    echo "$(date +"%Y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user USERNAME" >> /users/log.txt
fi
