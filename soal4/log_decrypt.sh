#!/bin/sh
hour=$(date +"%H")
alphabet="abcdefghijklmnopqrstuvwxyz"
shift=$((hour % 26))

# Check if the script was called with one argument
if [ $# -ne 1 ]
then
  echo "Usage: $0 [filename]"
  exit 1
fi

# Check if the specified file exists
if [ ! -f $1 ]
then
  echo "Error: File not found"
  exit 1
fi

encrypted_content=$(cat $1)
decrypted_content=$(echo "$encrypted_content" | tr "${alphabet:${shift}}${alphabet:0:${shift}}" "${alphabet}")
echo "$decrypted_content" | sudo tee /var/log/backup_log/dec_syslog > /dev/null
