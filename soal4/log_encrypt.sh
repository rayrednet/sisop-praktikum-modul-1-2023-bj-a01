#!/bin/sh
alphabet="abcdefghijklmnopqrstuvwxyz"
hour=$(date +"%H")
shift=$((hour % 26))

#memakai rhel os
content=$(cat /var/log/messages)
encrypted_content=$(echo "$content" | tr "${alphabet}" "${alphabet:${shift}}${alphabet:0:${shift}}")
echo "$encrypted_content" | tee /var/log/backup_log/encrypt/"$(date +%H:%M_%d:%m:%Y)".txt > /dev/null

#cronjob
#0 */2 * * * sh /var/log/backup_log/log_encrypt.sh